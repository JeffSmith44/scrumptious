from django import forms
from django.forms import ModelForm
from recipes.models import Recipe

class RecipeForm(forms.ModelForm):
    class Meta:
        model = Recipe
        fields = [
            "title",
            "picture",
            "description",
        ]
